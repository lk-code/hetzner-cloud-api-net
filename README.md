## hetzner-cloud-api for .net

the project is a .NET Standard 2.0 Library and can be used in the most projects like:

- Universal Windows Platform (UWP) (minimum target version is the fall creators update - https://blogs.msdn.microsoft.com/dotnet/2017/10/10/announcing-uwp-support-for-net-standard-2-0/)
- Xamarin and Xamarin.Forms (only .NET Standard)
- Windows Presentation Foundation (WPF)
- Console Application
- ASP.NET

### documentation

see the documentation on https://github.com/lk-code/hetzner-cloud-api-net/wiki